<?php

namespace Controller;

class Test extends Base {
  public function index($a) {
    return [
      'status'  => true,
      'user'  => [
        'username'  => 'test.test',
        'first_name'  => 'name'
      ]
    ];
  }
}