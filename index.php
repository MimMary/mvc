<?php

require_once 'vendor/autoload.php';

class APIController {
  public function __construct($URI) {
    $pattern = explode('/', $URI);
    $className = $pattern[1];
    if(isset($className) && $className) {
      $controllerPath = "Controller/{$className}.php";
      if(file_exists($controllerPath)) {
        require_once($controllerPath);  
        
        if(class_exists("Controller\\{$className}")) {
          $className      = "\Controller\\{$className}";
          $classInstance  = new $className();
          $methodName     = $pattern[2] ? $pattern[2] : 'index';

          if(method_exists($classInstance, $methodName)) {
            $reflection = new ReflectionClass($classInstance);

            $reflectionMethod = $reflection->getMethod($methodName);

            if($reflectionMethod->isPublic()) {
              
              $response = $reflectionMethod->invoke($classInstance);
              $this->response($response);
              exit;
            }

            exit;
          }
        }
      }
    }

    return $this->response([
      'status'  => false,
      'message' => 'not found'
    ]);
  }


  public function response($data) {
    header('Content-Type: application/json');
    echo json_encode($data, JSON_PRETTY_PRINT);
    exit;
  }
}

new APIController($_SERVER['REQUEST_URI']);
exit;

?>